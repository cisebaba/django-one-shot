from django.shortcuts import render
from todos.models import TodoItem, TodoList
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.urls import reverse_lazy
# Create your views here.


class TodoListListView(ListView):
    model = TodoList
    template_name = "todos_list/list.html"


class TodoListDetailView(DetailView):
    model= TodoList
    template_name = "todos_list/detail.html"


class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todos_list/new.html"
    fields = ["name"]
    def get_success_url(self):
        return reverse_lazy("todo_list_detail",args =[self.object.id])

class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name= "todos_list/edit.html"
    fields= ["name"]
    success_url = reverse_lazy("todo_list_list")

class TodoListDeleteView(DeleteView):
    model= TodoList
    template_name= "todos_list/delete.html"
    success_url = reverse_lazy("todo_list_list")

class TodoItemCreateView(CreateView):
    model= TodoItem
    template_name="todos_item/new.html"
    fields = ["task","due_date","is_completed","list"]
    
    def get_success_url(self):
        return reverse_lazy("todo_list_detail",args =[self.object.id])

class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "todos_item/edit.html"
    fields = ["task","due_date","is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail",args =[self.object.id])


